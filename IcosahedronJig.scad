//////////////////////////////////////////////////////////////////////////////
// This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
// Title of work: Icosahedron Jig
// Attribute work to name: Chris Molloy
// Attribute work to URL: https://chrismolloy.com/
// Format of work: Software
//////////////////////////////////////////////////////////////////////////////

include <../lib/polyhedra.scad>; // https://www.thingiverse.com/thing:3045257

// Length of each edge strut, ≈icosahedron radius (mm)
edgeLength = 50; // [50:500]

// Radius of each edge strut (mm)
strutRadius = 3/2; // [1:20]

/* [Hidden] */
$fn = 50;
zipTie = [2, 4, edgeLength]; // slot for zip tie
circumsphereRadius = (edgeLength / 4) * (sqrt(10 + (2 * sqrt(5))));
a = asin(edgeLength / (2 * circumsphereRadius));
jigRadiusOuter = min(edgeLength / 3, strutRadius * 15);
jigRadiusInner = strutRadius * 5;
zipRadius = ((jigRadiusOuter - jigRadiusInner) / 2) + jigRadiusInner;

module cutout() {
  rotate([a, 0, 0]) scale([edgeLength, edgeLength, edgeLength]) {
    for (ii = [0: len(icosahedron_edges) - 1]) {
      orient_edge(
        icosahedron_vertices[icosahedron_edges[ii][0]],
        icosahedron_vertices[icosahedron_edges[ii][1]]
      ) {
        cylinder(height = 1, r = strutRadius / edgeLength, center = true);
      }
    }
    polyhedron(
      points = icosahedron_vertices,
      faces = icosahedron_faces
    );
  }
}

translate([0, 0, edgeLength * 0.975]) difference() {
  translate([0, 0, -edgeLength * 0.85]) cylinder(r = jigRadiusOuter, h = edgeLength / 4, center = true);
  translate([0, 0, -edgeLength * 0.9]) cylinder(r = jigRadiusInner, h = edgeLength / 3, center = true);
  cutout();
  for (ii = [0: 4]) {
    rotate([0, 0, ii * 72]) translate([strutRadius + (zipTie[0] / 2), zipRadius, -edgeLength]) cube(zipTie, center = true);
    rotate([0, 0, ii * 72]) translate([-(strutRadius + (zipTie[0] / 2)), zipRadius, -edgeLength]) cube(zipTie, center = true);
  }
}

// Tests ('*' to hide; '%' to ghost)
translate([0, 0, edgeLength * 0.975]) {
  * sphere(r = circumsphereRadius);
  % cutout();
}