#  Icosahedron Jig
My mate, @jeremygold, and I wanted to design a welding/soldering jig for a wire-frame icosahedron. We were conscious of not building a jig that would be difficult to remove from inside the finished article. And, so...

On this jig, each strut is held in place with a zip-tie. The jig exists entirely outside the body being welded/soldered, so nothing need be removed from inside the finished object (zip-ties must be cut out, obviously).

Ideally, you'll print 12 of these jigs - one for each vertex -, but it is possible to use a single jig for the whole object.

The OpenSCAD file makes use of polyhedra.scad (https://www.thingiverse.com/thing:3045257 - 'Creative Commons - Public Domain Dedication') - you'll need to grab that library, and then update the reference at the top of this OpenSCAD file.

Included in the file set is a sample STL file for an icosahedron with an edge length of 50mm, and a strut diameter of 3mm.

"But why..?"
- "... do I specify a strut length - surely it doesn't matter?" - mostly true, but I wanted the model to be able to adapt when the strut length gets really short (the jig shrinks so as not to interfere with other vertices).
- "... are the zip-tie holes not perpendicular to the struts?" - it's simpler/cleaner to print if the holes are perpendicular to the base.

